FROM php:7.2-apache

#install all the system dependencies and enable PHP modules 
RUN docker-php-ext-install mysqli pdo pdo_mysql

#set our application folder as an environment variable
ENV APP_HOME /var/www/html

#change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

#change the web_root to laravel /var/www/html/public folder
RUN sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf

# enable apache module rewrite
RUN a2enmod rewrite

#copy source files and run composer
COPY . $APP_HOME

#create link to storage into public
RUN ln -s ${APP_HOME}/storage/app/public ${APP_HOME}/public/storage

#change ownership of our applications
RUN chown -R www-data:www-data $APP_HOME

RUN echo "file_uploads = On\nmemory_limit = 196M\npost_max_size = 196M\nupload_max_filesize = 128M\nmax_execution_time = 6000" >> /usr/local/etc/php/conf.d/uploads.ini \