<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     return redirect(RouteServiceProvider::HOME);
        // }
        if (Auth::guard($guard)->check()) {
            // the following 3 lines
            if (Auth::user()->nivel == 'u') {
                //Verifica qual a última url para redirecionar para a página correta
                $previousUrl = url()->previous();
                if(Str::endsWith($previousUrl, 'login')) {
                    return redirect('/home');
                }
                else if(Str::endsWith($previousUrl, 'licitacao-logar')  || Auth::user()->nivel == 'l'){            
                    return redirect('/licitacao/publico'); //redirect to admin panel
                }
            }
    
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
