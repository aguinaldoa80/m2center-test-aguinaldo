<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\Grupo;
use App\Models\CampanhaGrupo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CampanhaGrupoController extends Controller
{
    /**
     * @OA\Get(
     *   path="/campanha/3/grupos",
     *   summary="Lista de campanha com seus grupos.",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $campanha_grupos = CampanhaGrupo::all();
        return response(['result' => $campanha_grupos,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/campanha/1/grupos",
     * summary="Cadastro de grupos para campanha com status ativo ou não",
     * description="Vincula um grupo a uma campanha",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do vinculo",
     *    @OA\JsonContent(
     *       required={"campanha_id","grupo_id","campanha_ativa"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example=" 1"),
     *       @OA\Property(property="grupo_id", type="string", format="grupo_id", example="1"),
     *       @OA\Property(property="campanha_ativa", type="string", format="campanha_ativa", example="1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Este grupo  já esta cadastrado para esta campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vinculo realizado com sucesso para campanha")
     *        )
     *     )
     * )
     */
    public function store(Request $request, $campanha_id)
    {
        $campanha_grupo = CampanhaGrupo::where('campanha_id','=',$campanha_id)->where('campanha_ativa',1)->get();

        if(count($campanha_grupo) > 0 && ($request->get('campanha_ativa') == true)){
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Não é possível ativar mais de uma campanha.']);
        }

        $campanha_grupo_aux = CampanhaGrupo::where('campanha_id','=', $campanha_id)->where('grupo_id','=', $request->get('grupo_id'))->get();
        
        if(count($campanha_grupo_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Jà existe um vinculo entre esta campanha e este grupo de cidades.']);
        }
        $campanha_grupo = new CampanhaGrupo();
        $campanha_grupo->grupo_id = $request->get('grupo_id');
        $campanha_grupo->campanha_id = $campanha_id;
        $campanha_grupo->campanha_ativa = $request->get('campanha_ativa');
        $campanha_grupo->save();
        
        return response(['result' => $campanha_grupo,'message'=>'Vinculo de campanha com grupo de cidades realizado com sucesso.','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/campanha/1/grupos",
     * summary="Atualiza um vinculo entre campanha e grupo",
     * description="Atualiza um vinculo entre campanha e grupo, se já houver duplicidade com campanha ativa os mesmos serão atualizados com valor false",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do vinculo",
     *    @OA\JsonContent(
     *       required={"campanha_id","produto_id"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example="1"),
     *       @OA\Property(property="produto_id", type="string", format="produto_id", example="1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vinculo atualizado com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        if(is_null($request->get('campanha_id')) || is_null($request->get('grupo_id')) || is_null($request->get('campanha_ativa'))){
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Todos os campos são de preencimento obrigatório']);
        }
        if(!is_null($request->get('campanha_ativa'))){
            if(!is_numeric($request->get('campanha_ativa')))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Campanha ativa deve ser um 0 ou 1']);

            if(($request->get('campanha_ativa') < 0 || $request->get('campanha_ativa') > 1))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Campanha ativa deve ser um 0 ou 1']);
        }
        $campanha_grupo_old = CampanhaGrupo::find($id);
        if(is_null($campanha_grupo_old)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha ou grupo não encontrado.']);
        }

        $campanha = Campanha::find($request->get('campanha_id'));
        $grupo = Grupo::find($request->get('grupo_id'));
        if(is_null($campanha) || is_null($grupo)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha ou grupo não encontrado.']);
        }
        
        $campanha_grupo_ativo = CampanhaGrupo::where('campanha_id','!=',$request->get('campanha_id'))->where('campanha_ativa',1)
        ->where('grupo_id','=',$request->get('grupo_id'))->update(['campanha_ativa' => 0]);

        $campanha_grupo_ativo = CampanhaGrupo::where('campanha_id','=',$request->get('campanha_id'))->where('grupo_id','=',$request->get('grupo_id'))
        ->where('id','!=',$id)->delete();
        
        $campanha_grupo_old->fill($request->all());
        $campanha_grupo_old->touch();
        
        return response(['result' => $campanha_grupo_old,'message'=>'Vinculo de campanha com grupo de cidades atualizado com sucesso.','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/campanha/1",
     * summary="Deleta um vinculo entre grupo e campanha",
     * description="Deleta um grupo de uma campanha",
     * @OA\Response(
     *    response=404,
     *    description="Vinculo não encontrado na campanha",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto não encontrado na campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vinculo removido com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $campanha_grupo = CampanhaGrupo::find($id);
        if(is_null($campanha_grupo)){
            return response(['message'=>'Grupo de campanha não encontrada','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $campanha_grupo->delete();
        return response(['result' => $campanha_grupo,'message'=>'Grupo de campanha removido com sucesso','statusCode' => Response::HTTP_OK]);
    }
}
