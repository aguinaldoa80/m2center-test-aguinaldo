<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\Produto;
use App\Models\CampanhaProduto;
use App\Models\CampanhaGrupo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CampanhaProdutoController extends Controller
{
    /**
     * @OA\Get(
     *   path="/campanha/1/produtos",
     *   summary="Lista de campanha com seus produtos e caso tenho desconto cadastrado e campanha ativa já traz os valores dos itens com o devido desconto.",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index($campanha_id)
    {
        $campanha = Campanha::find($campanha_id);
        $campanha_grupo = CampanhaGrupo::where('campanha_id','=',$campanha->id)->where('campanha_ativa',1)->get();

        if(count($campanha_grupo) > 0){
            if(!is_null($campanha->desconto)){
                foreach($campanha->campanha_produto as $key => $item){
                    $item->produto->produto_preco = $item->produto->produto_preco - (($campanha->desconto->desconto / 100) * $item->produto->produto_preco);
                }
            }
        }
        if(is_null($campanha)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha com o id '.$campanha_id.' não encontrada']);
        }
        return response(['result' => $campanha,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/campanha/1/produtos",
     * summary="Cadastro de produtos para campanha",
     * description="Vincula um produto a uma campanha",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do vinculo",
     *    @OA\JsonContent(
     *       required={"campanha_id","produto_id"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example=" 1"),
     *       @OA\Property(property="produto_id", type="string", format="produto_id", example="1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta produto já esta cadastrado para esta campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto cadastrado com sucesso para campanha")
     *        )
     *     )
     * )
     */
    public function store(Request $request, $campanha_id)
    {   
        $campanha = Campanha::find($campanha_id);
        
        if(is_null($campanha)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha com o id '.$campanha_id.' não encontrada']);
        }

        $produto = Produto::find($request->get('produto_id'));
    
        if(is_null($produto)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Produto com o id '.$request->get('produto_id').' não encontrado']);
        }
        
        $campanha_produto_aux = CampanhaProduto::where('campanha_id','=', $campanha->id)->where('produto_id','=', $produto->id)->get();
        
        if(count($campanha_produto_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este produto já está cadastrado para esta campanha.']);
        }
        $campanha_produto = new CampanhaProduto();
        $campanha_produto->produto_id = $produto->id;
        $campanha_produto->campanha_id = $campanha->id;
        $campanha_produto->save();
        
        return response(['result' => $campanha_produto,'message'=>'Produto cadastrado com sucesso para a campanha '.$campanha->campanha_nome,'statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/campanha/3/produtos/2",
     * summary="Atualiza um vinculo entre campanha e produto",
     * description="Atualiza um vinculo entre campanha e produto",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do vinculo",
     *    @OA\JsonContent(
     *       required={"campanha_id","produto_id"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example="1"),
     *       @OA\Property(property="produto_id", type="string", format="produto_id", example="1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Este produto já esta cadastrado para esta campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vinculo atualizado com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $campanha_id, $produto_id)
    {
        $campanha = Campanha::find($campanha_id);
        
        if(is_null($campanha)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha com o id '.$campanha_id.' não encontrada']);
        }

        $produto_old = Produto::find($produto_id);
    
        if(is_null($produto_old)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Produto com o id '.$produto_id.' não encontrado']);
        }

        $produto = Produto::find($request->get('produto_id'));
    
        if(is_null($produto)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Produto com o id '.$produto_id.' não encontrado']);
        }
        
        $campanha_produto_aux = CampanhaProduto::where('campanha_id','=', $campanha->id)->where('produto_id','=', $produto->id)->get();
        
        if(count($campanha_produto_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este produto já está cadastrado para esta campanha.']);
        }

        $campanha_produto = CampanhaProduto::where('campanha_id','=', $campanha->id)->where('produto_id','=', $produto_old->id)->update(['produto_id' => $produto->id]);

        return response(['result' => $campanha_produto,'message'=>'Produto atualizado com sucesso para a campanha '.$campanha->campanha_nome,'statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/campanha/3/produtos/4",
     * summary="Deleta um vinculo entre produto e campanha",
     * description="Deleta um produto de uma campanha",
     * @OA\Response(
     *    response=404,
     *    description="Produto não encontrado na campanha",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto não encontrado na campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Vinculo removido com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($campanha_id, $produto_id)
    {
        
        $campanha_produto = CampanhaProduto::where('campanha_id','=', $campanha_id)->where('produto_id','=', $produto_id)->get();

        if(count($campanha_produto) > 0){
            $campanha_produto = CampanhaProduto::where('campanha_id','=', $campanha_id)->where('produto_id','=', $produto_id)->delete();
            return response(['result' => '','message'=>'Produto removido da campanha com sucesso','statusCode' => Response::HTTP_OK]);
        }
        return response(['message'=>'Produto não encontrado para campanha','statusCode' => Response::HTTP_NOT_FOUND]);
    }
}
