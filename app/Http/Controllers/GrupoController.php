<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class GrupoController extends Controller
{
    /**
     * @OA\Get(
     *   path="/grupos",
     *   summary="Lista de grupos",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $grupos = Grupo::all();
        return response(['result' => $grupos,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/grupos",
     * summary="Cadastro de grupo",
     * description="Efetua um cadastro de uma grupo",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados da grupo",
     *    @OA\JsonContent(
     *       required={"grupo_nome"},
     *       @OA\Property(property="grupo_nome", type="string", format="grupo_nome", example="grupo 1"),
    
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Cidade já um grupo com este nome",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta grupo já está cadastrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Grupo cadastrado com sucesso")
     *        )
     *     )
     * )
     */
    public function store(Request $request)
    {
        if(strlen($request->get('grupo_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome do grupo precisa ter 3 ou mais caractéres.']);
        
        $grupo = new Grupo();
        $grupo->fill($request->all());
        $grupo->grupo_nome = Str::upper($grupo->grupo_nome);

        $grupo_aux = Grupo::where('grupo_nome','=', $grupo->grupo_nome)->get();
        
        if(count($grupo_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este grupo já está cadastrado']);
        }

        $grupo->save();
        return response(['result' => $grupo,'message'=>'Grupo cadastrado com sucesso','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function show(Grupo $grupo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function edit(Grupo $grupo)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/grupos/1",
     * summary="Atualiza um grupo",
     * description="Altera os dados de um grupo",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do grupo",
     *    @OA\JsonContent(
     *       required={"grupo_nome","grupo_estado"},
     *       @OA\Property(property="grupo_nome", type="string", format="grupo_nome", example="grupo 1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade de nome",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Este grupo já está cadastrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Grupo atualizado com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        if(strlen($request->get('grupo_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome do grupo precisa ter 3 ou mais caractéres.']);
        
        $grupo = Grupo::find($id);
        if(is_null($grupo)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Grupo com id '.$id.' não encontrado']);
        }

        $grupo->fill($request->all());
        $grupo->grupo_nome = Str::upper($grupo->grupo_nome);

        $grupo_aux = Grupo::where('grupo_nome','=', $grupo->grupo_nome)->get()->first();
        
        if(!is_null($grupo_aux)){
            if($grupo_aux->id != $grupo->id)
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este grupo já está cadastrado']);
        }
        
        $grupo->touch();
        return response(['result' => $grupo,'message'=>'Grupo atualizado com sucesso','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/grupos/1",
     * summary="Deleta um grupo",
     * description="Deleta um grupo e todas suas referencias em outras tabelas",
     * @OA\Response(
     *    response=404,
     *    description="Grupo não encontrado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Grupo não encontrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Grupo removido com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $grupo = Grupo::find($id);
        if(is_null($grupo)){
            return response(['message'=>'Grupo não encontrado','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $grupo->delete();
        return response(['result' => $grupo,'message'=>'Grupo removida com sucesso','statusCode' => Response::HTTP_OK]);
    }
}
