<?php

namespace App\Http\Controllers;

use App\Models\Desconto;
use App\Models\Campanha;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class DescontoController extends Controller
{
    /**
     * @OA\Get(
     *   path="/descontos",
     *   summary="Lista de descontos",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $descontos = Desconto::all();
        return response(['result' => $descontos,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/descontos",
     * summary="Cadastro de desconto",
     * description="Efetua um cadastro de uma desconto para uma determinada campanha todos produtos desta campanha
     * receberao o desconto",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do desconto",
     *    @OA\JsonContent(
     *       required={"campanha_id","desconto"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example="desconto 1"),
     *       @OA\Property(property="desconto", type="float", format="desconto", example="5.00"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta desconto já está cadastrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Desconto cadastrado com sucesso")
     *        )
     *     )
     * )
     */
    public function store(Request $request)
    {
        if(is_null($request->get('campanha_id')))
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Campo campanha é obrigatório.']);
        
        $campanha = Campanha::find($request->get('campanha_id'));
        if(is_null($campanha)){
            return response(['message'=>'Campanha não encontrada','statusCode' => Response::HTTP_NOT_FOUND]);
        }

        if(is_null($request->get('desconto'))){
            $request['desconto'] = '0.0';
        }

        if(!is_null($request->get('desconto'))){
            if(!is_numeric($request->get('desconto')))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número']);

            if(!($request->get('desconto') > 0))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número maior que zero']);
        }
        $desconto = new Desconto();
        $desconto->fill($request->all());
        
        $desconto_aux = Desconto::where('campanha_id','=', $desconto->campanha_id)->first();

        if(!is_null($desconto_aux)){
            $desconto_aux->delete();
        }

        $desconto->save();
        return response(['result' => $desconto,'message'=>'Desconto cadastrado com sucesso','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Desconto  $desconto
     * @return \Illuminate\Http\Response
     */
    public function show(Desconto $desconto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Desconto  $desconto
     * @return \Illuminate\Http\Response
     */
    public function edit(Desconto $desconto)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/descontos/1",
     * summary="Atualiza um desconto",
     * description="Altera os dados de um desconto",
     * @OA\RequestBody(
     *    required=true,
     *    description="Atualiza os dados do desconto",
     *    @OA\JsonContent(
     *       required={"campanha_id","desconto"},
     *       @OA\Property(property="campanha_id", type="string", format="campanha_id", example="desconto 1"),
     *       @OA\Property(property="desconto", type="float", format="desconto", example="5.00"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Est desconto já está cadastrada nesta campanha")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Desconto atualizado com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        $desconto = Desconto::find($id);
        if(is_null($desconto)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Desconto com id '.$id.' não encontrado']);
        }

        $campanha = Campanha::find($request->get('campanha_id'));
        if(is_null($campanha)){
            return response(['message'=>'Campanha não encontrada','statusCode' => Response::HTTP_NOT_FOUND]);
        }

        if(is_null($request->get('desconto'))){
            $request['desconto'] = '0.0';
        }
        if(!is_null($request->get('desconto'))){
            if(!is_numeric($request->get('desconto')))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número']);

            if(!($request->get('desconto') > 0))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número maior que zero']);
        }
        
        $desconto->fill($request->all());
        
        $desconto_aux = Desconto::where('campanha_id','=', $desconto->campanha_id)->get()->first();
        
        if(!is_null($desconto_aux)){
            if($desconto_aux->id != $desconto->id)
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este desconto já está cadastrado']);
        }
        
        $desconto->touch();
        return response(['result' => $desconto,'message'=>'Desconto atualizado com sucesso','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/descontos/1",
     * summary="Deleta um desconto",
     * description="Deleta um desconto e todas suas referencias em outras tabelas",
     * @OA\Response(
     *    response=404,
     *    description="Desconto não encontrado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Desconto não encontrada")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Desconto removido com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $desconto = Desconto::find($id);
        if(is_null($desconto)){
            return response(['message'=>'Desconto não encontrado','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $desconto->delete();
        return response(['result' => $desconto,'message'=>'Desconto removido com sucesso','statusCode' => Response::HTTP_OK]);
    }
}