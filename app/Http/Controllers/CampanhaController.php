<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CampanhaController extends Controller
{
    /**
     * @OA\Get(
     *   path="/campanhas",
     *   summary="Lista de campanhas",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $campanhas = Campanha::all();
        return response(['result' => $campanhas,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/campanhas",
     * summary="Cadastro de Campanha",
     * description="Efetua um cadastro de uma Campanha",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados da Campanha",
     *    @OA\JsonContent(
     *       required={"campanha_nome"},
     *       @OA\Property(property="campanha_nome", type="string", format="campanha_nome", example="Campanha 1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta Campanha já está cadastrada")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Campanha cadastrada com sucesso")
     *        )
     *     )
     * )
     */
    public function store(Request $request)
    {
        if(strlen($request->get('campanha_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome da campanha precisa ter 3 ou mais caractéres.']);
        
        $campanha = new Campanha();
        $campanha->fill($request->all());
        $campanha->campanha_nome = Str::upper($campanha->campanha_nome);

        $campanha_aux = Campanha::where('campanha_nome','=', $campanha->campanha_nome)->get();
        
        if(count($campanha_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Esta campanha já está cadastrada']);
        }

        $campanha->save();
        return response(['result' => $campanha,'message'=>'Campanha cadastrado com sucesso','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Campanha  $campanha
     * @return \Illuminate\Http\Response
     */
    public function show(Campanha $campanha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Campanha  $campanha
     * @return \Illuminate\Http\Response
     */
    public function edit(Campanha $campanha)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/campanhas/1",
     * summary="Atualiza uma campanha",
     * description="Altera os dados de uma campanha",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados da campanha",
     *    @OA\JsonContent(
     *       required={"campanha_nome","campanha_estado"},
     *       @OA\Property(property="campanha_nome", type="string", format="campanha_nome", example="campanha 1"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta campanha já está cadastrada")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Campanha atualizada com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        if(strlen($request->get('campanha_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome da campanha precisa ter 3 ou mais caractéres.']);
        
        $campanha = Campanha::find($id);
        if(is_null($campanha)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Campanha com id '.$id.' não encontrado']);
        }
        
        $campanha->fill($request->all());
        $campanha->campanha_nome = Str::upper($campanha->campanha_nome);

        $campanha_aux = Campanha::where('campanha_nome','=', $campanha->campanha_nome)->get()->first();
        
        if(!is_null($campanha_aux)){
            if($campanha_aux->id != $campanha->id)
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Esta campanha já está cadastrada']);
        }
        
        $campanha->touch();
        return response(['result' => $campanha,'message'=>'Campanha atualizada com sucesso','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/campanhas/1",
     * summary="Deleta uma campanha",
     * description="Deleta uma campanha e todas suas referencias em outras tabelas",
     * @OA\Response(
     *    response=404,
     *    description="Campanha não encontrada",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Campanha não encontrada")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Campanha removida com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $campanha = Campanha::find($id);
        if(is_null($campanha)){
            return response(['message'=>'Campanha não encontrado','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $campanha->delete();
        return response(['result' => $campanha,'message'=>'Campanha removido com sucesso','statusCode' => Response::HTTP_OK]);
    }
}