<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ProdutoController extends Controller
{
    /**
     * @OA\Get(
     *   path="/produtos",
     *   summary="Lista de produtos",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $produtos = Produto::all();
        return response(['result' => $produtos,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/produtos",
     * summary="Cadastro de produto",
     * description="Efetua um cadastro de um produto",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do produto",
     *    @OA\JsonContent(
     *       required={"produto_descricao","produto_preco"},
     *       @OA\Property(property="produto_descricao", type="string", format="produto_descricao", example="cidade 1"),
     *       @OA\Property(property="produto_preco", type="float", format="produto_preco", example="1.00"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esto produto já está cadastrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto cadastrado com sucesso")
     *        )
     *     )
     * )
     */
    public function store(Request $request)
    {
        if(strlen($request->get('produto_descricao')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'A descricao do produto precisa ter 3 ou mais caractéres.']);
        
        if(is_null($request->get('produto_preco'))){
            $request['produto_preco'] = '0.0';
        }
        if(!is_null($request->get('produto_preco'))){
            if(!is_numeric($request->get('produto_preco')))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número']);

            if(!($request->get('produto_preco') > 0))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número maior que zero']);
        }
        $produto = new Produto();
        $produto->fill($request->all());
        $produto->produto_descricao = Str::upper($produto->produto_descricao);

        $produto_aux = Produto::where('produto_descricao','=', $produto->produto_descricao)->get();
        
        if(count($produto_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este produto já está cadastrado']);
        }

        $produto->save();
        return response(['result' => $produto,'message'=>'Produto cadastrado com sucesso','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/produtos/1",
     * summary="Atualiza uma prouto",
     * description="Altera os dados de uma cidade",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados do produto",
     *    @OA\JsonContent(
     *       required={"produto_descricao","produto_preco"},
     *       @OA\Property(property="produto_descricao", type="string", format="produto_descricao", example="cidade 1"),
     *       @OA\Property(property="produto_preco", type="float", format="produto_preco", example="1.00"),
     *    ),
     * ),
     * @OA\Response(
     *    response=404,
     *    description="Não encontrado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto id não encontrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=409,
     *    description="Duplicidade",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Este produto já esta cadastrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto atualizado com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        if(strlen($request->get('produto_descricao')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'A descricao do produto precisa ter 3 ou mais caractéres.']);
        
        $produto = Produto::find($id);
        if(is_null($produto)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Produto com id '.$id.' não encontrado']);
        }

        if(is_null($request->get('produto_preco'))){
            $request['produto_preco'] = '0.0';
        }
        if(!is_null($request->get('produto_preco'))){
            if(!is_numeric($request->get('produto_preco')))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número']);

            if(!($request->get('produto_preco') > 0))
                return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'Preço deve ser um número maior que zero']);
        }
        
        $produto->fill($request->all());
        $produto->produto_descricao = Str::upper($produto->produto_descricao);

        $produto_aux = Produto::where('produto_descricao','=', $produto->produto_descricao)->get()->first();
        
        if(!is_null($produto_aux)){
            if($produto_aux->id != $produto->id)
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Este produto já está cadastrado']);
        }
        
        $produto->touch();
        return response(['result' => $produto,'message'=>'Produto atualizado com sucesso','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/produtos/1",
     * summary="Deleta um produto",
     * description="Deleta um produto e todas suas referencias em outras tabelas",
     * @OA\Response(
     *    response=404,
     *    description="Não encontrado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto não encontrado")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Produto removido com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        if(is_null($produto)){
            return response(['message'=>'Produto não encontrado','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $produto->delete();
        return response(['result' => $produto,'message'=>'Produto removido com sucesso','statusCode' => Response::HTTP_OK]);
    }
}