<?php

namespace App\Http\Controllers;

use App\Models\Cidade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CidadeController extends Controller
{
    private $estados = array("AC","AL","AM","AP","BA","CE","DF","ES","GO","MA","MG","MS","MT","PA","PB","PE","PI","PR","RJ",
    "RN","RO","RR","RS","SC","SE","SP","TO");
    
    /**
     * @OA\Get(
     *   path="/cidades",
     *   summary="Lista de cidades",
     *   @OA\Response(response=200, description="successful operation")
     * )
     */
    public function index()
    {
        $cidades = Cidade::all();
        return response(['result' => $cidades,'message'=>'','statusCode' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     * path="/cidades",
     * summary="Cadastro de cidade",
     * description="Efetua um cadastro de uma cidade",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados da cidade",
     *    @OA\JsonContent(
     *       required={"cidade_nome","cidade_estado"},
     *       @OA\Property(property="cidade_nome", type="string", format="cidade_nome", example="cidade 1"),
     *       @OA\Property(property="cidade_estado", type="string", format="cidade_estado", example="MG"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Cidade já existe para o estado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta cidade já está cadastrada neste estado")
     *        )
     *     ),
     * @OA\Response(
     *    response=201,
     *    description="Cadastro ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Cidade cadastrada com sucesso")
     *        )
     *     )
     * )
     */
    public function store(Request $request)
    {
        if(strlen($request->get('cidade_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome da cidade precisa ter 3 ou mais caractéres.']);
        if(strlen($request->get('cidade_estado')) != 2)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'A sigla do estado precisa ter 2 caractéres.']);

        $cidade = new Cidade();
        $cidade->fill($request->all());
        $cidade->cidade_nome = Str::upper($cidade->cidade_nome);
        $cidade->cidade_estado = Str::upper($cidade->cidade_estado);

        if (!in_array($cidade->cidade_estado, $this->estados)) {
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Estado com a sigla '.$cidade->cidade_estado. ' não existe.']);
        }   

        $cidade_aux = Cidade::where('cidade_nome','=', $cidade->cidade_nome)->where('cidade_estado','=', $cidade->cidade_estado)->get();
        
        if(count($cidade_aux) > 0){
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Esta cidade já está cadastrada neste estado']);
        }

        $cidade->save();
        return response(['result' => $cidade,'message'=>'Cidade cadastrada com sucesso','statusCode' => Response::HTTP_CREATED]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function show(Cidade $cidade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cidade  $cidade
     * @return \Illuminate\Http\Response
     */
    public function edit(Cidade $cidade)
    {
        //
    }

    /**
     * @OA\Put(
     * path="/cidades/1",
     * summary="Atualiza uma cidade",
     * description="Altera os dados de uma cidade",
     * @OA\RequestBody(
     *    required=true,
     *    description="Dados da cidade",
     *    @OA\JsonContent(
     *       required={"cidade_nome","cidade_estado"},
     *       @OA\Property(property="cidade_nome", type="string", format="cidade_nome", example="cidade 1"),
     *       @OA\Property(property="cidade_estado", type="string", format="cidade_estado", example="MG"),
     *    ),
     * ),
     * @OA\Response(
     *    response=409,
     *    description="Cidade já existe para o estado",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Esta cidade já está cadastrada neste estado")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Atualização ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Cidade atualizada com sucesso")
     *        )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        if(strlen($request->get('cidade_nome')) < 3)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'O nome da cidade precisa ter 3 ou mais caractéres.']);
        if(strlen($request->get('cidade_estado')) != 2)
            return response(['statusCode' => Response::HTTP_FORBIDDEN, 'message'=>'A sigla do estado precisa ter 2 caractéres.']);

        $cidade = Cidade::find($id);
        if(is_null($cidade)){
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Cidade com id '.$id.' não encontrada']);
        }

        $cidade->fill($request->all());
        $cidade->cidade_nome = Str::upper($cidade->cidade_nome);
        $cidade->cidade_estado = Str::upper($cidade->cidade_estado);

        if (!in_array($cidade->cidade_estado, $this->estados)) {
            return response(['statusCode' => Response::HTTP_NOT_FOUND, 'message'=>'Estado com a sigla '.$cidade->cidade_estado. ' não existe.']);
        }   

        $cidade_aux = Cidade::where('cidade_nome','=', $cidade->cidade_nome)->where('cidade_estado','=', $cidade->cidade_estado)->get()->first();
        
        if(!is_null($cidade_aux)){
            if($cidade_aux->id != $cidade->id)
            return response(['statusCode' => Response::HTTP_CONFLICT, 'message'=>'Esta cidade já está cadastrada neste estado']);
        }
        
        $cidade->touch();
        return response(['result' => $cidade,'message'=>'Cidade atualizada com sucesso','statusCode' => Response::HTTP_OK]);
    }

    /**
     * @OA\Delete(
     * path="/cidades/1",
     * summary="Deleta uma cidade",
     * description="Deleta uma cidade e todas suas referencias em outras tabelas",
     * @OA\Response(
     *    response=404,
     *    description="Cidade não encontrada",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Cidade não encontrada")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="Remoção ok.",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Cidade removida com sucesso")
     *        )
     *     )
     * )
     */
    public function destroy($id)
    {
        $cidade = Cidade::find($id);
        if(is_null($cidade)){
            return response(['message'=>'Cidade não encontrada','statusCode' => Response::HTTP_NOT_FOUND]);
        }
        $cidade->delete();
        return response(['result' => $cidade,'message'=>'Cidade removida com sucesso','statusCode' => Response::HTTP_OK]);
    }
}
