<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampanhaGrupo extends Model
{
    protected $fillable = ['campanha_id','grupo_id','campanha_ativa'];

    protected $with = ['campanha'];

    public function campanha()
    {
        return $this->belongsTo('App\Models\Campanha');
    }
}
