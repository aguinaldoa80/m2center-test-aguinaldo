<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campanha extends Model
{
    protected $fillable = ['campanha_nome'];

    protected $with = ['campanha_produto'];

    public function campanha_produto()
    {
        return $this->hasMany('App\Models\CampanhaProduto');
    }

    public function desconto()
    {
        return $this->hasOne('App\Models\Desconto');
    }
}
