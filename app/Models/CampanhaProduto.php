<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampanhaProduto extends Model
{

    protected $fillable = ['campanha_id','produto_id'];

    protected $with = ['produto'];

    
    public function produto() 
    {
        return $this->belongsTo('App\Models\Produto','produto_id');
    }

}