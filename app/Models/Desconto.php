<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Desconto extends Model
{
    protected $fillable = ['campanha_id','desconto'];

    protected $with = ['campanha'];

    public function campanha()
    {
        return $this->belongsTo('App\Models\Campanha');
    }
}
