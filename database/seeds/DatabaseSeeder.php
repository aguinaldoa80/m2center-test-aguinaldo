<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(OrganizacaoSeeder::class);
        // $this->call(UsuarioSeeder::class);
        // $this->call(PortalSeeder::class);
        // $this->call(ProjetoModalidadeSeeder::class);
        // $this->call(TipoParceriaSeeder::class);
        // $this->call(SecretariasSeeder::class);
        // $this->call(FonteReceitaSeeder::class);
        // $this->call(ClassificacaoEconomicaSeeder::class);
        // $this->call(EspecieDocumentoSeeder::class);
        // $this->call(FornecedorSeeder::class);
        // $this->call(PrestadorServicoSeeder::class);
        // $this->call(UsuarioOrganizacaoSeeder::class);
    }
}
