<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampanhaGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanha_grupos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campanha_id');
            $table->unsignedInteger('grupo_id');
            $table->boolean('campanha_ativa')->default(FALSE);
            $table->foreign('campanha_id')
            ->references('id')
            ->on('campanhas')
            ->onDelete('cascade');
            $table->foreign('grupo_id')
            ->references('id')
            ->on('grupos')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanha_grupos');
    }
}
