<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampanhaProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanha_produtos', function (Blueprint $table) {
            //$table->increments('id');
            $table->unsignedInteger('campanha_id');
            $table->unsignedInteger('produto_id');
            $table->foreign('campanha_id')
            ->references('id')
            ->on('campanhas')
            ->onDelete('cascade');
            $table->foreign('produto_id')
            ->references('id')
            ->on('produtos')
            ->onDelete('cascade');
            $table->primary(['campanha_id', 'produto_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanha_produtos');
    }
}
