<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoCidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_cidades', function (Blueprint $table) {
            $table->unsignedInteger('cidade_id')->unique();
            $table->unsignedInteger('grupo_id');
            $table->foreign('cidade_id')
            ->references('id')
            ->on('cidades')
            ->onDelete('cascade');
            $table->foreign('grupo_id')
            ->references('id')
            ->on('grupos')
            ->onDelete('cascade');
            $table->primary(['cidade_id', 'grupo_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_cidades');
    }
}
