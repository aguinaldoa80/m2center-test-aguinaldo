<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('v1')->group(function () {
    Route::resource('cidades','CidadeController');
    Route::resource('grupos','GrupoController');
    Route::resource('produtos','ProdutoController');
    Route::resource('campanhas','CampanhaController');
    Route::resource('descontos','DescontoController');
    Route::prefix('campanha/{campanha_id}')->group(function () {
        Route::resource('produtos','CampanhaProdutoController');
        Route::resource('grupos','CampanhaGrupoController');
        Route::put('/grupos', [
            'uses' => 'CampanhaGrupoController@update'
        ]);
        Route::delete('', [
            'uses' => 'CampanhaGrupoController@destroy'
        ]);
    });
    
});

//Auth::routes();
